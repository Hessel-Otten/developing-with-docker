FROM node:14.17.5-alpine

ENV MONGO_DB_USERNAME=admin \
    MONGO_DB_PWD=password

# set default dir so that next commands executes in /home/app dir
WORKDIR /home/app

COPY ./app/package*.json .

# will execute npm install in /home/app because of WORKDIR
RUN npm install

COPY ./app .

EXPOSE 3000

# no need for /home/app/server.js because of WORKDIR
CMD ["node", "server.js"]
